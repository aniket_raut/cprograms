/* WAP to read radius of circle and calculate Area and Circumference */

#include <stdio.h>

int main() {
	int radius;
	float area;
	float circumference;
	
	printf("Enter radius ");
	scanf("%d", &radius);
	
	area = 3.14 * radius * radius;
	circumference = 2 * radius * radius;
	
	printf("\nArea of circle is %f", area);
	printf("\nCircumference of circle is %.2f", circumference);
	
	getch();
}
